﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace task
{
    public class Node<T>
    {
        public T Data { get; set; } // Дані вузла
        public Node<T> Previous { get; set; } // Попередній вузол
        public Node<T> Next { get; set; } // Наступний вузол

        public Node(T data)
        {
            Data = data;
        }
    }

    public class DoubleNodeList<T> : IEnumerable<T>
    {
        public Node<T> head; // Голова списку
        public Node<T> tail; // Хвіст списку
        public int size; // Розмір списку

        public bool IsEmpty { get { return size == 0; } } // Перевірка на порожній список

        public void PushFront(T data)
        {
            Node<T> node = new Node<T>(data); // Створення нового вузла
            Node<T> tmp = head;
            node.Next = tmp;
            head = node;
            if (size == 0)
            {
                tail = head;
            }
            else
            {
                tmp.Previous = node;
            }
            size++;
        }

        public void PushBack(T data)
        {
            Node<T> node = new Node<T>(data); // Створення нового вузла
            if (head == null)
            {
                head = node;
            }
            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            size++;
        }

        public bool DeleteNode(T data)
        {
            Node<T> node = head; // Починаємо з голови списку
            while (node != null)
            {
                if (node.Data.Equals(data)) // Якщо дані відповідають, то зупиняємося
                {
                    break;
                }
                node = node.Next; // Переходимо до наступного вузла
            }
            if (node != null) // Якщо вузол знайдено
            {
                if (node.Next != null)
                {
                    node.Next.Previous = node.Previous;
                }
                else
                {
                    tail = node.Previous;
                }
                if (node.Previous != null)
                {
                    node.Previous.Next = node.Next;
                }
                else
                {
                    head = node.Next;
                }
                size--;
                return true;
            }
            return false;
        }

        public void Clear()
        {
            head = null;
            tail = null;
            size = 0;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> node = head;
            while (node != null)
            {
                yield return node.Data;
                node = node.Next;
            }
        }

        // Сортування за вибором
    }

    internal class Program
    {
        public static void SelectListSort(DoubleNodeList<int> list)
        {
            var temp = list.head; // Запам'ятовуємо перший елемент
            while (temp != null)
            {
                var min = temp; // Присвоюємо мінімум
                var next = temp.Next;
                while (next != null) // Доки є елементи
                {
                    // Пошук мінімума
                    if (min.Data > next.Data)
                    {
                        min = next;
                    }
                    next = next.Next;
                }
                // Обмін першого і мінімального вузлів
                var tempExchange = temp.Data;
                temp.Data = min.Data;
                min.Data = tempExchange;
                // Переходимо далі
                temp = temp.Next;
            }
        }

        // Сортування вставкою для масиву
        public static void InsertArraySort(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                var temp = arr[i]; // Елемент у тимчасовій змінній
                int j;
                // Вставка у відсортовану частину
                for (j = i - 1; j >= 0; j--)
                {
                    if (arr[j] < temp)
                    {
                        break;
                    }
                    arr[j + 1] = arr[j];
                }
                // Вставляємо елемент на місце
                arr[j + 1] = temp;
            }
        }

        // Сортування вставкою для двозв'язного списку
        public static void InsertListSort(DoubleNodeList<int> list)
        {
            // Запам'ятовуємо перший елемент
            var elem = list.head;
            Node<int> nodeNow;
            Node<int> nodePrev;
            while (elem != null)
            {
                nodeNow = elem; // Поточний елемент
                nodePrev = nodeNow.Previous; // Попередній елемент
                while (nodePrev != null && nodeNow.Data < nodePrev.Data)
                {
                    // Обмін попереднього і поточного вузлів
                    int temp = nodePrev.Data;
                    nodePrev.Data = nodeNow.Data;
                    nodeNow.Data = temp;
                    // Переходимо далі
                    nodeNow = nodePrev;
                    nodePrev = nodePrev.Previous;
                }
                elem = elem.Next; // Переходимо до наступного вузла 
            }
        }

        static void Main(string[] args)
        {
            int menu;
            do
            {
                Stopwatch stopwatch = new Stopwatch();
                Random random = new Random();
                Console.OutputEncoding = Encoding.UTF8;

                Console.WriteLine("Оберіть пункт:\n1. Сортування вибором: двозв'язний список\n2. Сортування вибором: масив\n3. Сортування вставками: двозв'язний список\n0. Вихід\n ");
                if (!int.TryParse(Console.ReadLine(), out menu))
                {
                    Console.WriteLine("Такого пункту не існує");
                }
                switch (menu)
                {
                    case 1:
                        DoubleNodeList<int> listForSelectionSorting = new DoubleNodeList<int>();
                        Console.WriteLine("Сортування вибором: двозв'язний список");
                        for (int i = 0; i < 10; i++)
                        {
                            listForSelectionSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        SelectListSort(listForSelectionSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 10 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForSelectionSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 100; i++)
                        {
                            listForSelectionSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        SelectListSort(listForSelectionSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 100 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForSelectionSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 500; i++)
                        {
                            listForSelectionSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        SelectListSort(listForSelectionSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 500 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForSelectionSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 1000; i++)
                        {
                            listForSelectionSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        SelectListSort(listForSelectionSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 1000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForSelectionSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 2000; i++)
                        {
                            listForSelectionSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        SelectListSort(listForSelectionSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 2000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForSelectionSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 5000; i++)
                        {
                            listForSelectionSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        SelectListSort(listForSelectionSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 5000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForSelectionSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 10000; i++)
                        {
                            listForSelectionSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        SelectListSort(listForSelectionSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 10000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        Console.WriteLine("\n\n");
                        Console.ReadLine();
                        break;
                    case 2:
                        int[] array = new int[10];
                        Console.WriteLine("\nСортування вставками: масив");
                        for (int i = 0; i < array.Length; i++)
                        {
                            array[i] = random.Next(-1000, 1000);
                        }
                        stopwatch.Start();
                        InsertArraySort(array);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 10 елементів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        array = new int[100];
                        for (int i = 0; i < array.Length; i++)
                        {
                            array[i] = random.Next(-1000, 1000);
                        }
                        stopwatch.Start();
                        InsertArraySort(array);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 100 елементів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        array = new int[500];
                        for (int i = 0; i < array.Length; i++)
                        {
                            array[i] = random.Next(-1000, 1000);
                        }
                        stopwatch.Start();
                        InsertArraySort(array);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 500 елементів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        array = new int[1000];
                        for (int i = 0; i < array.Length; i++)
                        {
                            array[i] = random.Next(-1000, 1000);
                        }
                        stopwatch.Start();
                        InsertArraySort(array);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 1000 елементів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        array = new int[2000];
                        for (int i = 0; i < array.Length; i++)
                        {
                            array[i] = random.Next(-1000, 1000);
                        }
                        stopwatch.Start();
                        InsertArraySort(array);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 2000 елементів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        array = new int[5000];
                        for (int i = 0; i < array.Length; i++)
                        {
                            array[i] = random.Next(-1000, 1000);
                        }
                        stopwatch.Start();
                        InsertArraySort(array);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 5000 елементів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        array = new int[10000];
                        for (int i = 0; i < array.Length; i++)
                        {
                            array[i] = random.Next(-1000, 1000);
                        }
                        stopwatch.Start();
                        InsertArraySort(array);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 10000 елементів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        Console.WriteLine("\n\n");
                        Console.ReadLine();
                        break;
                    case 3:
                        DoubleNodeList<int> listForInsertSorting = new DoubleNodeList<int>();
                        Console.WriteLine("\nСортування вставками: двозв'язний список");
                        for (int i = 0; i < 10; i++)
                        {
                            listForInsertSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        InsertListSort(listForInsertSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 10 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForInsertSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 100; i++)
                        {
                            listForInsertSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        InsertListSort(listForInsertSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 100 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForInsertSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 500; i++)
                        {
                            listForInsertSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        InsertListSort(listForInsertSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 500 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForInsertSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 1000; i++)
                        {
                            listForInsertSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        InsertListSort(listForInsertSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 1000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForInsertSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 2000; i++)
                        {
                            listForInsertSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        InsertListSort(listForInsertSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 2000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForInsertSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 5000; i++)
                        {
                            listForInsertSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        InsertListSort(listForInsertSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 5000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        listForInsertSorting = new DoubleNodeList<int>();
                        for (int i = 0; i < 10000; i++)
                        {
                            listForInsertSorting.PushBack(random.Next(-1000, 1000));
                        }
                        stopwatch.Start();
                        InsertListSort(listForInsertSorting);
                        stopwatch.Stop();
                        Console.WriteLine($"Час для 10000 вузлів: \t{stopwatch.Elapsed.TotalMilliseconds} ms; ");
                        Console.WriteLine("\n\n");
                        Console.ReadLine();
                        break;
                }
            } while (menu != 0);
        }
    }
}
